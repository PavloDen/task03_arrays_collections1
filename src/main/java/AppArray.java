import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
/*
A. Дано два масиви. Сформувати третій масив, що складається з тих елементів, які:
        а) присутні в обох масивах;
        б) присутні тільки в одному з масивів.
B. Видалити в масиві всі числа, які повторюються більше двох разів.
C. Знайти в масиві всі серії однакових елементів, які йдуть підряд, і видалити з них всі елементи крім одного.
 */
public class AppArray {
  private static final int MIN_NUMBER = 1;
  private static final int MAX_NUMBER = 22;
  private static final int ARRAY_LENGTH = 11;

  public static void main(String[] args) {
    List<Integer> firstArray = new ArrayList<>();
    List<Integer> secondArray = new ArrayList<>();
    for (int i = 0; i < ARRAY_LENGTH; i++) {
      firstArray.add(ThreadLocalRandom.current().nextInt(MIN_NUMBER, MAX_NUMBER + 1));
      secondArray.add(ThreadLocalRandom.current().nextInt(MIN_NUMBER, MAX_NUMBER + 1));
    }
    Collections.sort(firstArray);
    Collections.sort(secondArray);

    System.out.println("firstArray = " + firstArray);
    System.out.println("secondArray = " + secondArray);

    Set<Integer> uniqueFirstArray = new HashSet<>(firstArray);
    Set<Integer> uniqueSecondArray = new HashSet<>(secondArray);
    System.out.println(uniqueFirstArray);
    System.out.println(uniqueSecondArray);

    Set<Integer> intersection = new HashSet<>(uniqueFirstArray);
    intersection.retainAll(uniqueSecondArray);
    System.out.println("Intersection = " + intersection);

    Set<Integer> complementFirstToSecond = new HashSet<>(uniqueFirstArray);
    complementFirstToSecond.removeAll(uniqueSecondArray);
    System.out.println("Relative complement of first in second " + complementFirstToSecond);

    Set<Integer> complementSecondToFirst = new HashSet<>(uniqueSecondArray);
    complementSecondToFirst.removeAll(uniqueFirstArray);
    System.out.println("Relative complement of second in first " + complementSecondToFirst);
  }
}
