package model;

public class Artifact extends Something {

  public Artifact() {}

  public Artifact(int power) {
    super(power);
  }

  @Override
  public String toString() {
    return "Artifact { " + super.toString() + " }";
  }
}
