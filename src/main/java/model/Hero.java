package model;

public class Hero {
  private int power;

  public Hero(int power) {
    this.power = power;
  }

  public int getPower() {
    return power;
  }

  public void setPower(int power) {
    this.power = power;
  }

  @Override
  public String toString() {
    return "Hero{" + "power=" + power + '}';
  }
}
