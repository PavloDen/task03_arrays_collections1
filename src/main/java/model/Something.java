package model;

public abstract class Something {

  private int power;

    public Something() {
    }

    public Something(int power) {
    this.power = power;
  }

  public int getPower() {
    return power;
  }

  public void setPower(int power) {
    this.power = power;
  }

  @Override
  public String toString() {
    return "{" + "power=" + power + '}';
  }
}
