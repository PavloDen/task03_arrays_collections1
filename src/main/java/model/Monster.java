package model;

public class Monster extends Something {

    public Monster() {
    }

    public Monster(int power) {
    super(power);
  }

  @Override
  public String toString() {
    return "Monster { " + super.toString() + " }";
  }
}
