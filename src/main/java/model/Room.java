package model;

public class Room {
    private int number;
    private Something smth;
    private boolean isVisited;

    public Room(int number, Something smth, boolean isVisited) {
        this.number = number;
        this.smth = smth;
        this.isVisited = isVisited;
    }

    public boolean isVisited() {
        return isVisited;
    }

    public void setVisited(boolean visited) {
        isVisited = visited;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public Something getSmth() {
        return smth;
    }

    public void setSmth(Something smth) {
        this.smth = smth;
    }

    @Override
    public String toString() {
        return "Room{" +
                "number=" + number +
                ", smth=" + smth +
                ", isVisited=" + isVisited +
                "} \n";
    }
}
