import model.Artifact;
import model.Monster;
import model.Room;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class GameCalculation {
  public static int getRandom(int min, int max) {
    int randomNum = ThreadLocalRandom.current().nextInt(min, max + 1);
    return randomNum;
  }

  public static List<Room> initializeRooms(int numberOfRooms) {
    List<Room> rooms = new ArrayList<>();
    Random random = new Random();
    for (int i = 0; i < numberOfRooms; i++) {
      if (random.nextBoolean()) {
        Monster monster = new Monster(getRandom(5, 100));
        Room room = new Room(i, monster, false);
        rooms.add(room);
      } else {
        Artifact artifact = new Artifact(getRandom(10, 80));
        Room room = new Room(i, artifact, false);
        rooms.add(room);
      }
    }
    return rooms;
  }


}
