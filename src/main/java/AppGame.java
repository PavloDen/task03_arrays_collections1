import model.Artifact;
import model.Hero;
import model.Monster;
import model.Room;

import java.util.ArrayList;
import java.util.List;

public class AppGame {

  /*
  D. Герой комп'ютерної гри, що володіє силою в 25 балів, знаходиться в круглому залі, з якого ведуть 10 закритих дверей.
  За кожними дверима героя чекає або магічний артефакт, що дарує силу від 10 до 80 балів, або монстр,
  який має силу від 5 до 100 балів, з яким герою потрібно битися. Битву виграє персонаж, що володіє найбільшою силою;
  якщо сили рівні, перемагає герой.
  1. Організувати введення інформації про те, що знаходиться за дверима, або заповнити її,
     використовуючи генератор випадкових чисел.
  2. Вивести цю саму інформацію на екран в зрозумілому табличному вигляді.
  3. Порахувати, за скількома дверима героя чекає смерть. Рекурсивно.
  4. Вивести номери дверей в тому порядку, в якому слід їх відкривати герою, щоб залишитися в живих, якщо таке можливо.
     */

  private static final int NUMBER_OF_ROOMS = 10;

  public static void main(String[] args) {

    Hero hero = new Hero(25);
    System.out.println(hero + " was created");

    List<Room> rooms = new ArrayList<>();
    rooms = GameCalculation.initializeRooms(NUMBER_OF_ROOMS);
    System.out.println(rooms);
    int inputMenu = -1;
    while (inputMenu != 0) {
      System.out.println(
          "Game Menu\n 1. Open Room\n 2. Count Death\n 3. Calculate path for Win\n 0.Exit\n    pls enter your choice ?");
      inputMenu = ConsoleInput.getInteger();
      if (inputMenu == 1) {
        for (Room r : rooms) {
          if (!r.isVisited()) {
            System.out.println("Room " + r.getNumber() + " is not visited");
          }
        }
        int roomNumber = 0;
        while (roomNumber < 10) {
          System.out.println("pls, enter number of room (for exit enter 10)");
          roomNumber = ConsoleInput.getInteger();
          if ((roomNumber >= 10) || (roomNumber < 0)) {
            break;
          }
          if (rooms.get(roomNumber).isVisited()) {
            System.out.println("Room " + roomNumber + " is visited");
          } else {
            rooms.get(roomNumber).setVisited(true);
            System.out.println("You are in Room " + rooms.get(roomNumber));
            if (rooms.get(roomNumber).getSmth() instanceof Artifact) {
              hero.setPower(hero.getPower() + rooms.get(roomNumber).getSmth().getPower());
              System.out.println("Power was updated\n" + hero);
            }
            if (rooms.get(roomNumber).getSmth() instanceof Monster) {
              if (rooms.get(roomNumber).getSmth().getPower() > hero.getPower()) {
                System.out.println("Hero is dead\nGame Over");
                hero.setPower(0);
                break;
              } else {
                System.out.println("You won Monster\n Congrats!!!");
              }
            }
          }
        }
      }
      if (inputMenu == 2) {
        System.out.println("In " + countDeath(rooms, hero) + " rooms hero meets death");
      }
      if (inputMenu == 3) {
        List<Integer> winPath = new ArrayList<>();
        int heroPower = hero.getPower();
        for (Room r : rooms) {
          if ((!r.isVisited()) && (r.getSmth() instanceof Artifact)) {
            heroPower = heroPower + r.getSmth().getPower();
            winPath.add(r.getNumber());
          }
        }
        for (Room r : rooms) {
          if ((!r.isVisited()) && (r.getSmth() instanceof Monster)) {
            if (r.getSmth().getPower() <= heroPower) {
              winPath.add(r.getNumber());
            } else {
              winPath = null;
              break;
            }
          }
        }
        if (winPath == null) {
          System.out.println("Sorry, no win path");
        } else {
          System.out.println(" Win path for hero ->> " + winPath);
        }
      }

      if ((inputMenu == 0) || (hero.getPower() <= 0)) {
        System.out.println("Bye-bye");
        break;
      }
    }
  }

  private static int countDeath(List<Room> rooms, Hero hero) {
    //      private static int countDeathFunction(int count){
    //          if (count<0) return 0;
    //          if ((r.getSmth() instanceof Monster)&&(r.getSmth().getPower() > hero.getPower()))
    //          return count + countDeathFunction(count-1);
    //      }
    int deathCounter = 0;
    //    deathCounter = countDeathFunction(NUMBER_OF_ROOMS);
    for (Room r : rooms) {
      if ((r.getSmth() instanceof Monster) && (r.getSmth().getPower() > hero.getPower())) {
        deathCounter++;
      }
    }
    return deathCounter;
  }
}
