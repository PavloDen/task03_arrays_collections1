import java.util.Scanner;

public class ConsoleInput {
  public static Integer getInteger() {
    String strInput;
    Scanner keyboard = new Scanner(System.in);
    boolean notAnInteger = true;
    int result = -1;
    while (notAnInteger) {
      strInput = keyboard.next();
      try {
        result = Integer.parseInt(strInput);
        notAnInteger = false;
      } catch (NumberFormatException e) {
        System.out.println("Not an integer");
        System.out.println(e);
      }
    }
    return result;
  }
}
